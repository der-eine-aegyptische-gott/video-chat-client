import React, {useEffect,useState} from 'react';
import MainComponent from "./components/MainComponent";
import {initiateSocket} from "./services/Socket";

export default function App() {

  let socket = initiateSocket();
  return (
      <MainComponent socket={socket}/>
  );
}

