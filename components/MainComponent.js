import React, {useEffect} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import ChatInputComponent from "./ChatInputComponent";

export default function MainComponent(props) {

    return(
        <View style={styles.container}>
            <Text >This is the Voice-Chat-Client View</Text>
            <ChatInputComponent socket={props.socket}/>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});


