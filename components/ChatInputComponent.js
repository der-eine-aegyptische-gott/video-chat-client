import React, {useState} from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';

export default function ChatInputComponent(props) {
    const [message, setMessage] = useState("");
    const onPress = () => {
        props.socket.emit('chat message', message)
        setMessage("");
    };
    return(
        <View>
            <TextInput
                placeholder={"Chat Message"}
                autoCapitalize={"words"}
                onChangeText = {chatMessage => setMessage(chatMessage)}
                value={message}
            />
            <TouchableOpacity
                style={styles.button}
                onPress={onPress}
            >
                <Text>
                    Send Message!
                </Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    button: {
        alignItems: "center",
        backgroundColor: "#656565",
        padding: 10
    },
});


